<?php

namespace AppBundle\Tests\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostControllerTest extends WebTestCase
{
    public function testCountPostByTag()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/post/tag/count/nack');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertGreaterThanOrEqual(0, $client->getResponse()->getContent());
    }

    public function testCountPostByTags()
    {
        $client = static::createClient();
        $crawler = $client->request('POST', '/api/post/tags/count', array('tags' => array('11', 'chayapol')));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertGreaterThanOrEqual(0, $client->getResponse()->getContent());
    }

    public function testCreateFailedPost()
    {
        $client = static::createClient();
        $crawler = $client->request('POST', '/api/post');

        $this->assertEquals(406, $client->getResponse()->getStatusCode());
    }

    public function testCRUDSuccessPost()
    {
        $createClient = static::createClient();
        $createClient->enableProfiler();
        $createClient->request('POST', '/api/post',
            array(
                "title" => "test title",
                "body" => "test body",
                "tags" => array(
                    "test",
                    "test2"
                )
            )
        );

        $mailCollector = $createClient->getProfile()->getCollector('swiftmailer');

        // Check that an email was sent
        $this->assertEquals(1, $mailCollector->getMessageCount());
        $postObject = json_decode($createClient->getResponse()->getContent());
        $this->assertEquals(200, $createClient->getResponse()->getStatusCode());

        $updateClient = static::createClient();
        $updateClient->request('PUT', '/api/post/'. $postObject->id,
            array(
                "title" => "test title update",
                "body" => "test body update",
                "tags" => array(
                    "test update",
                    "test2 update"
                )
            )
        );
        $this->assertEquals(200, $updateClient->getResponse()->getStatusCode());

        $deleteClient = static::createClient();
        $deleteClient->request('DELETE', '/api/post/'. $postObject->id);
        $this->assertEquals(200, $deleteClient->getResponse()->getStatusCode());
    }
}
