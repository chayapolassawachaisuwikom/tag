<?php

namespace AppBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Posts;
use AppBundle\Entity\Tags;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\Common\Collections\ArrayCollection;

/**
/**
 *  @ApiDoc(
 *     description="Delete tag by id.",
 *  )
 * @Route("/api/tag")
 */
class TagController extends FOSRestController
{
    /**
     *  @ApiDoc(
     *     section="Tag",
     *     description="Retrieve list of tags."
     *  )
     * @Rest\Get("/")
     */
    public function getAction()
    {
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Tags')->findAll();
        if ($restresult === null) {
            return new View("there are no tag exist", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     *  @ApiDoc(
     *     section="Tag",
     *     description="Get tag by id."
     *  )
     * @Rest\Get("/{id}")
     */
    public function idAction($id)
    {
        $singleresult = $this->getDoctrine()->getRepository('AppBundle:Tags')->find($id);
        if ($singleresult === null) {
            return new View("tag not found", Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }

    /**
     * @ApiDoc(
     *  section="Tag",
     *  description="Create a new Tag",
     *  requirements={
     *      {
     *          "name"="name",
     *          "dataType"="string",
     *          "requirement"="text",
     *          "description"="name for tag"
     *      }
     *  }
     * )
     * @Rest\Post("/")
     */
    public function postAction(Request $request)
    {
        $data = new Tags();

        $name = $request->get('name');

        if(empty($name))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }

        $data->setName($name);

        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View("Tag Added Successfully", Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  section="Tag",
     *  description="Update the Tag",
     *  requirements={
     *      {
     *          "name"="name",
     *          "dataType"="string",
     *          "requirement"="text",
     *          "description"="name for tag"
     *      }
     *  }
     * )
     * @Rest\Put("/{id}")
     */
    public function updateAction($id,Request $request)
    {
        $name = $request->get('name');
        $sn = $this->getDoctrine()->getManager();
        $tag = $this->getDoctrine()->getRepository('AppBundle:Tags')->find($id);
        if (empty($tag)) {
            return new View("tag not found", Response::HTTP_NOT_FOUND);
        }
        if(!empty($name)){

            $tag->setName($name);
            $sn->flush();
            return new View("Tag Updated Successfully", Response::HTTP_OK);
        }
        else return new View("Name cannot be empty", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     *  @ApiDoc(
     *     section="Tag",
     *     description="Delete tag by id.",
     *  )
     * @Rest\Delete("/{id}")
     */
    public function deleteAction($id)
    {
        $sn = $this->getDoctrine()->getManager();
        $tag = $this->getDoctrine()->getRepository('AppBundle:Tags')->find($id);
        if (empty($tag)) {
            return new View("tag not found", Response::HTTP_NOT_FOUND);
        }
        else {
            $sn->remove($tag);
            $sn->flush();
        }
        return new View("deleted successfully", Response::HTTP_OK);
    }
}
