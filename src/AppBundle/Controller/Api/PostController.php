<?php

namespace AppBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Posts;
use AppBundle\Entity\Tags;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\Common\Collections\ArrayCollection;

/**
/**
 *  @ApiDoc(
 *     description="Delete post by id.",
 *  )
 * @Route("/api/post")
 */
class PostController extends FOSRestController
{
    /**
     *  @ApiDoc(
     *     section="Get post data",
     *     description="Retrieve list of posts.",
     *     responseMap={
     *         200 = "AppBundle\Entity\Posts"
     *     }
     *  )
     * @Rest\Get("")
     */
    public function getAction()
    {
//        $cacheDriver = new ApcCache();
        $restResult = $this->getDoctrine()->getRepository('AppBundle:Posts')->findAllOrderedById();
        if ($restResult === null) {
            return new View("there are no post exist", Response::HTTP_NOT_FOUND);
        }
        return $restResult;
    }

    /**
     *  @ApiDoc(
     *     section="Get post data",
     *     description="Get post by id.",
     *     responseMap={
     *         200 = "AppBundle\Entity\Posts"
     *     }
     *  )
     * @Rest\Get("/{id}")
     */
    public function idAction($id)
    {
        $singleResult = $this->getDoctrine()->getRepository('AppBundle:Posts')->find($id);
        if ($singleResult === null) {
            return new View("post not found", Response::HTTP_NOT_FOUND);
        }
        return $singleResult;
    }

    /**
     *  @ApiDoc(
     *    section="Get post data",
     *    description="Get post by tag",
     *    responseMap={
     *        200 = "AppBundle\Entity\Posts"
     *    }
     *  )
     * @Rest\Get("/tag/{tag}")
     */
    public function getByTagAction($tag)
    {
        $restResult = $this->getDoctrine()->getRepository('AppBundle:Posts')->getByTag($tag);

        return $restResult;
    }

    /**
     *  @ApiDoc(
     *    section="Get post data",
     *    description="Get number of post by tag",
     *    responseMap={
     *        200 = "integer"
     *    }
     *  )
     * @Rest\Get("/tag/count/{tag}")
     */
    public function countPostByTagAction($tag)
    {
        $restResult = $this->getDoctrine()->getRepository('AppBundle:Posts')->countPostByTag($tag);

        return $restResult;
    }

    /**
     *  @ApiDoc(
     *    section="Get post data",
     *    description="Get number of post by tags",
     *    responseMap={
     *        200 = "AppBundle\Entity\Posts"
     *    },
     *    parameters={
     *        {"name"="tags[0]", "dataType"="string", "required"=false, "description"="tag for search"},
     *        {"name"="tags[1]", "dataType"="string", "required"=false, "description"="tag for search"},
     *        {"name"="tags[2]", "dataType"="string", "required"=false, "description"="tag for search"},
     *        {"name"="tags[3]", "dataType"="string", "required"=false, "description"="tag for search"},
     *    }
     *  )
     * @Rest\Post("/tags/count")
     */
    public function countPostByTagsAction(Request $request)
    {
        $tags = $request->get('tags');
        $restResult = $this->getDoctrine()->getRepository('AppBundle:Posts')->countPostByTags($tags);

        return $restResult;
    }

    /**
     *  @ApiDoc(
     *    section="Get post data",
     *    description="Get posts by tags",
     *    responseMap={
     *        200 = "Integer"
     *    },
     *    parameters={
     *        {"name"="tags[0]", "dataType"="string", "required"=false, "description"="tag for search"},
     *        {"name"="tags[1]", "dataType"="string", "required"=false, "description"="tag for search"},
     *        {"name"="tags[2]", "dataType"="string", "required"=false, "description"="tag for search"},
     *        {"name"="tags[3]", "dataType"="string", "required"=false, "description"="tag for search"},
     *    }
     *  )
     * @Rest\Post("/tags")
     */
    public function getTagsAction(Request $request)
    {
        $tags = $request->get('tags');
        $restResult = $this->getDoctrine()->getRepository('AppBundle:Posts')->getByTags($tags);

        return $restResult;
    }

    /**
     * @ApiDoc(
     *  section="Manage post",
     *  description="Create a new Post",
     *  responseMap={
     *      200 = "AppBundle\Entity\Posts"
     *  },
     *  requirements={
     *      {
     *          "name"="title",
     *          "dataType"="string",
     *          "requirement"="text",
     *          "description"="title for post"
     *      },
     *      {
     *          "name"="body",
     *          "dataType"="string",
     *          "requirement"="text",
     *          "description"="body for post"
     *      },
     *  },
     *  parameters={
     *      {"name"="tags[0]", "dataType"="string", "required"=false, "description"="collection of tags"},
     *      {"name"="tags[1]", "dataType"="string", "required"=false, "description"="collection of tags"},
     *      {"name"="tags[2]", "dataType"="string", "required"=false, "description"="collection of tags"},
     *      {"name"="tags[3]", "dataType"="string", "required"=false, "description"="collection of tags"},
     *  }
     * )
     * @Rest\Post("")
     */
    public function postAction(Request $request)
    {
        $data = new Posts();

        $title = $request->get('title');
        $body = $request->get('body');
        $tags = $request->get('tags');
        if(empty($title) || empty($body))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        if(!empty($tags)){
            foreach($tags as $tag){
                $tagObject = $this->getDoctrine()->getRepository('AppBundle:Tags')->findOneBy(array("name"=>$tag));
                if (empty($tagObject)) {
                    $tagObject = new Tags();
                    $tagObject->setName($tag);
                }
                $data->addTag($tagObject);
            }
        }
        $data->setTitle($title);
        $data->setBody($body);

        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();

        $message = \Swift_Message::newInstance()
            ->setSubject("Someone create the post !!")
            ->setFrom($this->getParameter("sender_address"))
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'AppBundle:Emails:post_created.txt.twig',
                    array('data' => $data)
                ),
                'text/plain'
            )
        ;
        $this->get('mailer')->send($message);

        return new View($data, Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  section="Manage post",
     *  description="Update the Post",
     *  requirements={
     *      {
     *          "name"="title",
     *          "dataType"="string",
     *          "requirement"="text",
     *          "description"="title for post"
     *      },
     *      {
     *          "name"="body",
     *          "dataType"="string",
     *          "requirement"="text",
     *          "description"="body for post"
     *      },
     *  },
     *  parameters={
     *      {"name"="tags[0]", "dataType"="string", "required"=false, "description"="collection of tags"},
     *      {"name"="tags[1]", "dataType"="string", "required"=false, "description"="collection of tags"},
     *      {"name"="tags[2]", "dataType"="string", "required"=false, "description"="collection of tags"},
     *      {"name"="tags[3]", "dataType"="string", "required"=false, "description"="collection of tags"},
     *  }
     * )
     * @Rest\Put("/{id}")
     */
    public function updateAction($id,Request $request)
    {
        $title = $request->get('title');
        $body = $request->get('body');
        $tags = $request->get('tags');
        $sn = $this->getDoctrine()->getManager();
        $post = $this->getDoctrine()->getRepository('AppBundle:Posts')->find($id);
        if (empty($post)) {
            return new View("post not found", Response::HTTP_NOT_FOUND);
        }
        if(!empty($title) && !empty($body)){
            $originalTags = new ArrayCollection();
            if (!empty($tags)) {
                foreach ($tags as $tag) {
                    $tagObject = $this->getDoctrine()->getRepository('AppBundle:Tags')->findOneBy(array("name"=>$tag));
                    if (empty($tagObject)) {
                        $tagObject = new Tags();
                        $tagObject->setName($tag);
                    }
                    $originalTags->add($tagObject);
                }
            }
            $post->removeAllTag();
            foreach ($originalTags as $tag) {
                $post->addTag($tag);
            }

            $post->setTitle($title);
            $post->setBody($body);
            $sn->flush();
            return new View("Post Updated Successfully", Response::HTTP_OK);
        }
        else return new View("Body and title cannot be empty", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     *  @ApiDoc(
     *     section="Manage post",
     *     description="Delete post by id.",
     *  )
     * @Rest\Delete("/{id}")
     */
    public function deleteAction($id)
    {
        $sn = $this->getDoctrine()->getManager();
        $post = $this->getDoctrine()->getRepository('AppBundle:Posts')->find($id);
        if (empty($post)) {
            return new View("post not found", Response::HTTP_NOT_FOUND);
        }
        else {
            $sn->remove($post);
            $sn->flush();
            $logger = $this->get('deleted_post.logger');
            $logger->info("Post id $id has been deleted");
        }
        return new View("deleted successfully", Response::HTTP_OK);
    }
}
